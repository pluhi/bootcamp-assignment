package com.cgi;

import java.util.Scanner;

public class GameEngine {
    private Scanner input = new Scanner(System.in);
    Player player = new Player(input.next(), input.next());

    public void start() {
        System.out.println(player.toString());

        hospoda1:
        while (true) {

            System.out.println("-- STARTING GAME --");
            System.out.println("Select an option:\n (a) Start Game \n (b) Exit ");

            char selection = input.next().charAt(0);
            input.nextLine();

            switch (selection) {
                case 'a' -> {
                    System.out.println("The game is begin...");
                    break hospoda1;
                }
                case 'b' -> System.exit(0);
                default -> System.out.println("Invalid action.");
            }
        }
    }

    public void hospoda1() {

        System.out.println("-- You are in HOSPODA 1 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a', 'd', 'c' -> hospoda1();
            case 'b' -> hospoda2();
            default -> {
                System.out.println("Invalid selection.");
                hospoda1();
            }
        }
    }

    public void hospoda2() {

        System.out.println("-- You are in HOSPODA 2 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a' -> hospoda3();
            case 'b' -> hospoda2();
            case 'c' -> {
                System.out.println("You're return to HOSPODA 1");
                hospoda1();
            }
            case 'd' -> hospoda8();
            default -> {
                System.out.println("Invalid selection.");
                hospoda2();
            }
        }
    }

    public void hospoda3() {

        System.out.println("-- You are in HOSPODA 3 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a' -> {
                System.out.println("You're return to HOSPODA 2");
                hospoda2();
            }
            case 'b', 'd' -> hospoda3();
            case 'c' -> hospoda4();
            default -> {
                System.out.println("Invalid selection.");
                hospoda3();
            }
        }
    }

    public void hospoda4() {

        System.out.println("-- You are in HOSPODA 4 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a', 'c', 'b' -> hospoda4();
            case 'd' -> hospoda5();
            default -> {
                System.out.println("Invalid selection.");
                hospoda4();
            }
        }
    }

    public void hospoda5() {

        while (true) {
            System.out.println("-- You are in HOSPODA 5 --");
            System.out.println("Select an option: a | b | c | d");

            char selection = input.next().charAt(0);
            input.nextLine();

            switch (selection) {
                case 'a' -> {
                    System.out.println("CONGRATULATIONS " + player.getName() + "! You are finally at home and going to sleep. :)");
                    System.exit(0);
                }
                case 'b' -> hospoda6();
                case 'c' -> hospoda5();
                case 'd' -> hospoda8();
                default -> {
                    System.out.println("Invalid selection.");
                    hospoda5();
                }
            }
        }
    }

    public void hospoda6() {

        System.out.println("-- You are in HOSPODA 6 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a', 'd' -> hospoda6();
            case 'b' -> {
                System.out.println("You're return to HOSPODA 5");
                hospoda5();
            }
            case 'c' -> hospoda7();
            default -> {
                System.out.println("Invalid selection.");
                hospoda6();
            }
        }
    }

    public void hospoda7() {

        System.out.println("-- You are in HOSPODA 7 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a', 'c', 'd' -> hospoda7();
            case 'b' -> {
                System.out.println("You're return to HOSPODA 6");
                hospoda6();
            }
            default -> {
                System.out.println("Invalid selection.");
                hospoda7();
            }
        }
    }

    public void hospoda8() {

        System.out.println("-- You are in HOSPODA 8 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a' -> {
                System.out.println("You're return to HOSPODA 7");
                hospoda7();
            }
            case 'b' -> hospoda9();
            case 'c' -> {
                System.out.println("CONGRATULATIONS " + player.getName() + "! You are drunk, but you are finally at home. :)");
                System.exit(0);
            }
            case 'd' -> hospoda8();
            default -> {
                System.out.println("Invalid selection.");
                hospoda8();
            }
        }
    }

    public void hospoda9() {

        System.out.println("-- You are in HOSPODA 9 --");
        System.out.println("Select an option: a | b | c | d");

        char selection = input.next().charAt(0);
        input.nextLine();

        switch (selection) {
            case 'a', 'c' -> hospoda9();
            case 'b' -> {
                System.out.println("You're return to HOSPODA 8");
                hospoda8();
            }
            case 'd' -> {
                System.out.println("I am sorry " + player.getName() + ", You are drunk. Unfortunately, you haven't gotten home anymore :(");
                System.exit(0);
            }
            default -> {
                System.out.println("Invalid selection.");
                hospoda9();
            }
        }
    }
}








package com.cgi;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GameTest {


    @Test
    public void player() {
        Player player = new Player("a", "b");
        player.setName("ca");
        player.setNickname("gg");

        assertEquals(player.getName(), "ca");
        assertEquals(player.getNickname(), "gg");
    }
}



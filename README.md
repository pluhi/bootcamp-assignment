Táto hra stručne reprezentuje túru medzi jednotlivými krčmami. K absolvovaní jednej miestnosti treba si vybrať správnu možnosť. Dá sa vybrať medzi možnosťami a, b, c, d. Pri niektorých prípadoch sa hráč rozhodne vrátiť sa do predošlej krčmy. Zámerom hry sa dostať domov. Mapu cesty je možné nájsť v obrázke gameMap.png. Štruktúra hry nie je lineárna, takže sa z niektorých krčiem sa dostanete niekde uplne inde, ako by ste čakali.
Najdôležitejšou triedou je GameEngine, ktorá využíva prevážne switch/case funkciu. 

Aplikácia je kompilovaná s verziou javy 14. Prajem ti príjemnú zábavu. 
